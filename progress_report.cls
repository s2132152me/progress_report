% 進捗報告会用の報告書と中間審査・最終審査の梗概（summaryオプションを付ける）
\ProvidesClass{progress_report}[2021/02/20 Class for progress reports and summaries.]
\LoadClass[paper=a4, %
           fontsize=10pt, %
           head_space=20mm, %
           foot_space=20mm, %
           line_length=25zw, %
           twocolumn, %
           lualatex %
          ]{jlreq}

%% 追加のパッケージ
% フォント周りの設定
\usepackage[no-math]{fontspec}
\usepackage[deluxe]{luatexja-preset}

% 数式
\usepackage{amsmath,amssymb}
\usepackage{bm}
\allowdisplaybreaks[4]

% ブックマーク
\usepackage[unicode, hidelinks, hypertexnames=false, pdfusetitle]{hyperref}

% 番号付き箇条書き
\usepackage[shortlabels]{enumitem}

% 参考文献
\usepackage[numbers]{natbib}
\usepackage{url} % jecon.bstでURLの表示に使用する
\renewcommand{\refname}{\normalsize 参考文献}
\renewcommand{\bibfont}{\small}

% 賢いreference
% amsmath → hyperref → cleveref → autonum の順でusepackageする必要あり
\usepackage[nameinlink]{cleveref}

% 参照される数式のみに番号を振る
\usepackage{autonum}

% ソースコード用の環境
\usepackage{listings}
\usepackage{xcolor}

%% cleverefの設定
\crefname{equation}{式}{式}% {環境名}{単数形}{複数形} \crefで引くときの表示
\crefname{figure}{図}{図}
\crefname{table}{表}{表}
\crefname{equation}{式}{式}
\crefname{algorithm}{アルゴリズム}{アルゴリズム}
\crefname{lstlisting}{リスト}{リスト}
\crefname{appendix}{付録}{付録}
\crefname{enumi}{}{}
\crefname{enumii}{}{}
\crefname{enumiii}{}{}
\newcommand{\crefpairconjunction}{および}
\newcommand{\crefrangeconjunction}{〜}
\newcommand{\crefmiddleconjunction}{，}
\newcommand{\creflastconjunction}{，および}

% thesisの場合とprogress_report, summaryの場合とで「章」をchapterにするかsectionにするか変える
\crefname{section}{}{}
\creflabelformat{section}{#2#1章#3}
\crefname{subsection}{}{}
\creflabelformat{subsection}{#2#1節#3}
\crefname{subsubsection}{}{}
\creflabelformat{subsubsection}{#2#1項#3}

%% listingsの設定
\def\lstlistingname{リスト}
\def\lstlistlistingname{リスト目次}
\definecolor{commentsColor}{rgb}{0.497495, 0.497587, 0.497464}
\definecolor{keywordsColor}{rgb}{0.000000, 0.000000, 0.635294}
\definecolor{stringColor}{rgb}{0.558215, 0.000000, 0.135316}
\lstset{ %
  aboveskip=0.2\zh,
  alsoletter={()[].=},
  basicstyle=\ttfamily\footnotesize,
  belowskip=0.2\zh,
  breakatwhitespace=false,
  breaklines=true,
  columns=fullflexible,
  commentstyle=\color{commentsColor}\textit,
  classoffset=1,
  frame=tb,
  framesep=5pt,
  keywordstyle=\color{keywordsColor}\bfseries,
  language=Python,
  lineskip=-0.5ex,
  linewidth=\linewidth,
  numbers=left,
  numberstyle=\scriptsize\color{commentsColor},
  rulecolor=\color{black},
  showlines=false,
  showstringspaces=false,
  stepnumber=1,
  stringstyle=\color{stringColor},
  tabsize=2,
  xleftmargin=2em
}

\lstdefinelanguage{JavaScript}{
  keywords={typeof, new, true, false, catch, function, return, null, catch, switch, var, if, in, while, do, else, case, break},
  keywordstyle=\color{keywordsColor}\bfseries,
  ndkeywords={class, export, boolean, throw, implements, import, this},
  ndkeywordstyle=\color{keywordsColor}\bfseries,
  identifierstyle=\color{black},
  sensitive=false,
  comment=[l]{//},
  morecomment=[s]{/*}{*/},
  commentstyle=\color{commentsColor}\ttfamily,
  stringstyle=\color{stringColor}\ttfamily,
  morestring=[b]',
  morestring=[b]"
}

%% float前後のスペースを狭めに変更
% floatとfloatの間
\setlength\floatsep{0.4\zh}
% 本文とfloatの間
\setlength\textfloatsep{2\floatsep}
% 本文中のfloat
\setlength\intextsep{\textfloatsep}
% キャプションの上の余白
\setlength{\abovecaptionskip}{0.2\zh}

%% ページ番号を表示しない
\pagestyle{empty}

%% jsarticleとの互換性のためのコマンド
\newcommand\BibTeX{\textsc{Bib}\TeX}

%% 梗概オプション（summary）の設定
\newif\if@summary  % 新しいBoolean @summary を作成
\@summaryfalse     % @summaryにfalseを代入
\DeclareOption{summary}{\@summarytrue}
\ProcessOptions\relax

%% section, subsection, subsubsectionの行取りとフォントを設定
\ModifyHeading{section}{lines=1.5, font=\large\sffamily\gtfamily}
\ModifyHeading{subsection}{lines=1.3, font=\sffamily\gtfamily}
\ModifyHeading{subsubsection}{lines=1.2, font=\sffamily\gtfamily}

%% 梗概の場合は全体的に詰める
\if@summary
    % 見出しの行取りを詰める
    \ModifyHeading{section}{lines=1.1}
    \ModifyHeading{subsection}{lines=1}
    \ModifyHeading{subsubsection}{lines=1}
    % floatの前後を詰める
    \setlength\floatsep{0.2\zh}
    \setlength\textfloatsep{\floatsep}
    \setlength\intextsep{\textfloatsep}
    \setlength{\abovecaptionskip}{0.1\zh}
\fi

%% 箇条書きの設定
% ラベルとの間隔を少し空ける
\jlreqsetup{
    itemization_label_length={1\zw, i=2\zw},
    itemization_labelsep=0.3\zw
}
\setlength{\leftmargini}{2.3\zw}
\setlength{\leftmarginii}{1.3\zw}

% 前後の空きを詰める
\jlreqsetup{itemization_beforeafter_space=0.2\baselineskip}

%% 引用（quote, quotation, verse）と定理（theorem）の前後の空きを詰める
\jlreqsetup{
    quote_beforeafter_space=0.2\baselineskip,
    theorem_beforeafter_space=0.2\baselineskip
    }

%% 図表のキャプションのフォントを平体に設定
\jlreqsetup{caption_label_font=\rmfamily}
\jlreqsetup{caption_font=\rmfamily}

%% abstract環境の再定義
\renewenvironment{abstract}{%
\ifnum0\if@twocolumn\else1\fi\ifjlreq@preamble1\fi>0
  \ifjlreq@preamble
    \ifx l\jlreq@engine\else
      \def\jlreq@parhook@abstract{\futurelet\jlreq@nextchar\jlreq@openbracket@hook}%
      \PushPostHook{par}{\jlreq@parhook@abstract}%
    \fi
  \fi
  % とりあえず全部ボックスに入れる．
  \global\setbox\jlreq@abstractbox=\vtop\bgroup
  \ifjlreq@preamble
    % preamble後っぽく振る舞う
    \@noskipsecfalse
    % \@nodocumentが入っているので一時的に無効化
    \everypar{}%
\fi
  \if@twocolumn
    \parindent=0pt
    \hsize=\textwidth
    % 後で\@maketitleとともに呼び出されるときは\twcolumn[***]で呼び出される．
    \begin{minipage}[b]{\textwidth}%
  \fi
  \bgroup
  \small
  \list{\sffamily\abstractname\hskip0.5\zw}{%
    \listparindent\parindent
    \itemindent\listparindent
    \labelwidth\z@
    \labelsep\z@
    \leftmargin=2\jlreq@zw
    \rightmargin\leftmargin
    \@tempdima=1\jlreq@zw
    \@tempdimb=\dimexpr\linewidth - \@totalleftmargin - \leftmargin - \rightmargin\relax
    \@tempdimc=\@tempdimb
    \divide\@tempdimb by \@tempdima\multiply\@tempdimb by \@tempdima
    \ifdim\@tempdimb=\@tempdimc\else\@tempdimb=\dimexpr\@tempdimb - 1\jlreq@zw\relax\fi
    \leftmargin=\dimexpr\leftmargin + (\@tempdimc - \@tempdimb)/2\relax
    \rightmargin=\dimexpr\rightmargin + (\@tempdimc - \@tempdimb)/2\relax
    \parsep\z@ \@plus.1\jlreq@zh
  }%
  \item\relax
\else
  \section*{\abstractname}%
\fi
}{%
\ifnum0\if@twocolumn\else1\fi\ifjlreq@preamble1\fi>0
  \endlist
  \egroup
  %\vspace{\baselineskip}%
  \if@twocolumn
    \end{minipage}
  \fi
  \egroup
\fi
\ifjlreq@preamble
  \ifx l\jlreq@engine\else
    \def\jlreq@parhook@abstract{}%
  \fi
\else
  %\unvbox\jlreq@abstractbox
\fi
}

\renewcommand{\maketitle}{
    \begingroup
    \renewcommand\thefootnote{\@fnsymbol\c@footnote}%
    \def\@makefnmark{\rlap{\@textsuperscript{\normalfont\@thefnmark}}}%
    \long\def\@makefntext##1{\advance\leftskip 3zw
        \parindent 1zw\noindent
        \llap{\@textsuperscript{\normalfont\@thefnmark}\hskip0.3zw}##1}%
    \twocolumn[\@maketitle]%
    \@thanks
    \endgroup
    \setcounter{footnote}{0}%
    \global\let\thanks\relax
    \global\let\maketitle\relax
    \global\let\@thanks\@empty
    \global\let\@author\@empty
    \global\let\@date\@empty
    \global\let\@title\@empty
    \global\let\@subtitle\@empty % サブタイトル
    \global\let\@keyword\@empty % キーワード
    \global\let\@affiliation\@empty % 所属
    \global\let\@grade\@empty % 学年
    \global\let\@email\@empty % email
    \global\let\title\relax
    \global\let\subtitle\relax % サブタイトル
    \global\let\keyword\relax % キーワード
    \global\let\affiliation\relax % 所属
    \global\let\studentid\relax % 学生番号
    \global\let\grade\relax % 学年
    \global\let\email\relax % email
    \global\let\author\relax
    \global\let\date\relax
    \global\let\and\relax
}
\def\@maketitle{%
    \newpage\null
    \centering
    \let\footnote\thanks
    {\Large \textsf{\@title} \par}%
    \ifx\@subtitle\undefined
    \else
        {\large \textsf{\@subtitle} \par}% サブタイトル
    \fi
    \if@summary
    \else
        \vskip .5em
    \fi
    {
        \lineskip .5em
        \if@summary
            \begin{tabular}[t]{ccc}%
                \@studentid & \@author & （指導教員：國宗 永佳 教授）\\ % 所属・学年・著者名
            \end{tabular}\par
        \else
            \begin{tabular}[t]{ccc}%
                \@affiliation & \@grade & \@author % 所属・学年・著者名
            \end{tabular}\par
        \fi
    }%
    \ifx\@email\undefined
    \else
        \if@summary
        \else
            \texttt{\@email}\par
        \fi
    \fi

    \if@summary
    \else
        \@date\vskip 0.5\zh
    \fi

    \if@summary
    \else
        \ifvoid\jlreq@abstractbox
        \else\centerline{\box\jlreq@abstractbox}
        \fi
    \fi

    \ifx\@keyword\undefined
    \else
        \if@summary
        \else
            \begin{minipage}[b]{\textwidth}
                \small
                \list{\sffamily キーワード\hskip 0.5\zh}{
                    \listparindent\parindent
%                    \itemindent \listparindent
                    \itemindent\listparindent
                    \labelwidth\z@
                    \labelsep\z@
                    \leftmargin=1\jlreq@zw
                    \rightmargin\leftmargin
                    \@tempdima=1\jlreq@zw
                    \@tempdimb=\dimexpr\linewidth - \@totalleftmargin - \leftmargin - \rightmargin\relax
                    \@tempdimc=\@tempdimb
                    \divide\@tempdimb by \@tempdima\multiply\@tempdimb by \@tempdima
                    \ifdim\@tempdimb=\@tempdimc\else\@tempdimb=\dimexpr\@tempdimb - 1\jlreq@zw\relax\fi
                    \leftmargin=\dimexpr\leftmargin + (\@tempdimc - \@tempdimb)/2\relax
                    \rightmargin=\dimexpr\rightmargin + (\@tempdimc - \@tempdimb)/2\relax
                    \parsep\z@ \@plus.1\jlreq@zh
                }
                \item\relax\@keyword%
            \end{minipage}
            \vskip 0.5em
        \fi
    \fi
    % このクラスファイルで追加したタイトル要素の定義
    \gdef\@subtitle{}
    \gdef\@keyword{}
    \gdef\@affiliation{}
    \gdef\@studentid{}
    \gdef\@grade{}
    \gdef\@email{}
}

%% このクラスファイルで追加したタイトル要素の定義
\long\def\subtitle#1{\long\gdef\@subtitle{#1}}
\long\def\keyword#1{\long\gdef\@keyword{#1}}
\long\def\affiliation#1{\long\gdef\@affiliation{#1}}
\long\def\studentid#1{\long\gdef\@studentid{#1}}
\long\def\grade#1{\long\gdef\@grade{#1}}
\long\def\email#1{\long\gdef\@email{#1}}
